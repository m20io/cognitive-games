package com.assessment.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by mfrank on 3/4/14.
 */
public class BoxView extends SurfaceView implements SurfaceHolder.Callback {
    private DrawThread drawThread;
    private Runnable returnMethod;
    private SurfaceHolder surfaceHolder;
    public BoxView(Context context) {
        super(context);
        init();
    }

    public BoxView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public BoxView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();

    }

    private void init() {
        getHolder().addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {
        this.surfaceHolder = surfaceHolder;
        if(drawThread == null){
            drawThread = new DrawThread();
            drawThread.setRunning(true);
            drawThread.start();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        drawThread.setRunning(false);
        boolean retry = true;
        while(retry) {
            try {
                drawThread.join();
                retry = false;
            } catch (InterruptedException e) {}
        }
    }

    public void setReturnMethod(Runnable runnable) {
        this.returnMethod = runnable;
    }

    public void doDraw() {
        Canvas c = surfaceHolder.lockCanvas();
        c.drawColor(Color.WHITE);
        surfaceHolder.unlockCanvasAndPost(c);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    class DrawThread extends Thread {
        private boolean running;

        @Override
        public void run() {
            while(running) {
                doDraw();
                returnMethod.run();
            }
        }

        public void setRunning(boolean running) {
            this.running = running;
        }
    }

}
