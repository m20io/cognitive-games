package com.assessment.controller;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import com.assessment.R;
import com.assessment.view.BoxView;

/**
 * Created by mfrank on 3/4/14.
 */
public class GameLauncherActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start);
    }

    public void startGame(View v) {
        BoxView boxView = new BoxView(GameLauncherActivity.this);
        boxView.setReturnMethod(new Runnable() {
            @Override
            public void run() {
                GameLauncherActivity.this.gameFinished();
            }
        });
        setContentView(boxView);
    }

    public void gameFinished() {
        runOnUiThread( new Runnable() {
            @Override
            public void run() {
                setContentView(R.layout.start);
            }
        });
    }
}
